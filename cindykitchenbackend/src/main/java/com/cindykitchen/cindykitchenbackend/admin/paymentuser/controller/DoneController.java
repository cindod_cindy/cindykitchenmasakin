package com.cindykitchen.cindykitchenbackend.admin.paymentuser.controller;

import com.cindykitchen.cindykitchenbackend.admin.paymentuser.exception.ResourceNotFoundException;
import com.cindykitchen.cindykitchenbackend.admin.paymentuser.model.Done;
import com.cindykitchen.cindykitchenbackend.admin.paymentuser.repository.DoneRepository;
import com.cindykitchen.cindykitchenbackend.admin.paymentuser.repository.VerificationRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping("/cindykitchen/api/donepayment")
public class DoneController {
    @Autowired
    private DoneRepository doneRepository;

    @Autowired
    private VerificationRepository verificationRepository;

    @GetMapping("getAllDone")
    public List<Done> getAllDone(){
        return doneRepository.findAll();
    }

    @PostMapping("/verification/{verificationId}/getdone")
    public Page<Done> getAllDoneByVerificationId(@PathVariable (value = "verificationId") Long verificationId,
                                         Pageable pageable) {
        return doneRepository.findByVerificationId(verificationId, pageable);
    }

    @PostMapping("/verification/{verificationId}/postdone")
    public Done createDone(@PathVariable (value = "verificationId") Long verificationId,
                                 @Valid @RequestBody Done done) {
        return verificationRepository.findById(verificationId).map(verification -> {
            done.setVerification(verification);
            return doneRepository.save(done);
        }).orElseThrow(() -> new ResourceNotFoundException("verificationId " + verificationId + " not found"));
    }
}
