package com.cindykitchen.cindykitchenbackend.admin.week.repository;

import com.cindykitchen.cindykitchenbackend.admin.week.model.Week;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface WeekRepository extends JpaRepository<Week, Long> {
}
