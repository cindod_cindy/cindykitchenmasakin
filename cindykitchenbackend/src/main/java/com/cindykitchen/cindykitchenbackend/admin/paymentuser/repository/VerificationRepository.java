package com.cindykitchen.cindykitchenbackend.admin.paymentuser.repository;

import com.cindykitchen.cindykitchenbackend.admin.paymentuser.model.Payment;
import com.cindykitchen.cindykitchenbackend.admin.paymentuser.model.Verification;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;

public interface VerificationRepository extends JpaRepository<Verification,Long> {
    Page<Verification> findByPaymentId(Long paymentId, Pageable pageable);
}
