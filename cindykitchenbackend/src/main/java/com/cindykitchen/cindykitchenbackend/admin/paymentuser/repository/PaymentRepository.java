package com.cindykitchen.cindykitchenbackend.admin.paymentuser.repository;

import com.cindykitchen.cindykitchenbackend.admin.paymentuser.model.Payment;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface PaymentRepository extends JpaRepository<Payment, Long> {
}
