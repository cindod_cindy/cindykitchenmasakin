package com.cindykitchen.cindykitchenbackend.admin.paymentuser.controller;

import com.cindykitchen.cindykitchenbackend.admin.paymentuser.exception.ResourceNotFoundException;
import com.cindykitchen.cindykitchenbackend.admin.paymentuser.model.Done;
import com.cindykitchen.cindykitchenbackend.admin.paymentuser.model.Warn;
import com.cindykitchen.cindykitchenbackend.admin.paymentuser.repository.DoneRepository;
import com.cindykitchen.cindykitchenbackend.admin.paymentuser.repository.WarnRepository;
import com.cindykitchen.cindykitchenbackend.auth.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping("/cindykitchen/api/warnpayment")
public class WarnController {

    @Autowired
    private WarnRepository warnRepository;

    @Autowired
    private DoneRepository doneRepository;

    @PostMapping("/adminGetAllWarnUser")
    public List<Warn> getAllWarnToUser(){
        return warnRepository.findAll();

    }

    @PostMapping("/done/{doneId}/getwarn")
    public Page<Warn> getAllWarnByUserId(@PathVariable (value = "doneId") Long doneId,
                                         Pageable pageable) {
        return warnRepository.findByDoneId(doneId, pageable);
    }

    @PostMapping("/done/{doneId}/postwarn")
    public Warn createWarn(@PathVariable (value = "doneId") Long doneId,
                           @Valid @RequestBody Warn warn) {
        return doneRepository.findById(doneId).map(done -> {
            warn.setDone(done);
            return warnRepository.save(warn);
        }).orElseThrow(() -> new ResourceNotFoundException("doneId " + doneId + " not found"));
    }
}
