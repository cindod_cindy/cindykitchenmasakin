package com.cindykitchen.cindykitchenbackend.admin.paymentuser.model;

import com.cindykitchen.cindykitchenbackend.auth.models.User;
import com.fasterxml.jackson.annotation.JsonIgnore;
import javax.persistence.*;
import javax.validation.constraints.NotNull;
import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;

@Entity
@Table(name = "payments")
public class Payment  extends AuditModel{

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @NotNull
    private String username;

    @NotNull
    private String bankpengirim;

    @NotNull
    private String namapengirim;

    @NotNull
    private String jumlahUang;

    public Payment() {
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getBankpengirim() {
        return bankpengirim;
    }

    public void setBankpengirim(String bankpengirim) {
        this.bankpengirim = bankpengirim;
    }

    public String getNamapengirim() {
        return namapengirim;
    }

    public void setNamapengirim(String namapengirim) {
        this.namapengirim = namapengirim;
    }

    public String getJumlahUang() {
        return jumlahUang;
    }

    public void setJumlahUang(String jumlahUang) {
        this.jumlahUang = jumlahUang;
    }
}
