package com.cindykitchen.cindykitchenbackend.admin.week.model;

import com.cindykitchen.cindykitchenbackend.admin.paymentuser.model.AuditModel;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

@Entity
@Table(name = "weeks")
public class Week extends AuditModel {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @NotNull
    @Size(max = 250)
    private String week_variable;

    public Week() {
    }

    public Week(String week_variable) {
        this.week_variable = week_variable;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getWeek_variable() {
        return week_variable;
    }

    public void setWeek_variable(String week_variable) {
        this.week_variable = week_variable;
    }
}
