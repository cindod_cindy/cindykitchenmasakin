package com.cindykitchen.cindykitchenbackend.admin.paymentuser.repository;

import com.cindykitchen.cindykitchenbackend.admin.paymentuser.model.Done;
import com.cindykitchen.cindykitchenbackend.admin.paymentuser.model.Warn;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface WarnRepository extends JpaRepository<Warn, Long> {
    Page<Warn> findByDoneId(Long doneId, Pageable pageable);
}
