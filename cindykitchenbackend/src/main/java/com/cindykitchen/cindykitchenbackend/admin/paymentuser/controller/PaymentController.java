package com.cindykitchen.cindykitchenbackend.admin.paymentuser.controller;

import com.cindykitchen.cindykitchenbackend.admin.paymentuser.exception.ResourceNotFoundException;
import com.cindykitchen.cindykitchenbackend.admin.paymentuser.model.Payment;
import com.cindykitchen.cindykitchenbackend.admin.paymentuser.repository.PaymentRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@RestController
@RequestMapping("/cindykitchen/api/payment")
public class PaymentController {

    @Autowired
    private PaymentRepository paymentRepository;

    @PostMapping("/getpayment")
    public Page<Payment> getAllPayment(Pageable pageable) {
        return paymentRepository.findAll(pageable);
    }

    @PostMapping("/postpayment")
    public Payment createPayment(@Valid @RequestBody Payment payment) {
        return paymentRepository.save(payment);
    }


    @PostMapping("/editpayments/{paymentId}")
    public Payment updatePayment(@PathVariable Long paymentId, @Valid @RequestBody Payment payment) {
        return paymentRepository.findById(paymentId).map(payments -> {
            payments.setUsername(payment.getUsername());
            payments.setBankpengirim(payment.getBankpengirim());
            payments.setNamapengirim(payment.getNamapengirim());
            payments.setJumlahUang(payment.getJumlahUang());
            return paymentRepository.save(payments);
        }).orElseThrow(() -> new ResourceNotFoundException("PaymentId " + paymentId + " not found"));
    }

}
