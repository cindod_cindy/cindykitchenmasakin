package com.cindykitchen.cindykitchenbackend.admin.paymentuser.controller;

import com.cindykitchen.cindykitchenbackend.admin.paymentuser.exception.ResourceNotFoundException;
import com.cindykitchen.cindykitchenbackend.admin.paymentuser.model.Verification;
import com.cindykitchen.cindykitchenbackend.admin.paymentuser.repository.PaymentRepository;
import com.cindykitchen.cindykitchenbackend.admin.paymentuser.repository.VerificationRepository;
import com.cindykitchen.cindykitchenbackend.auth.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping("/cindykitchen/api/verification")
public class VerificationController {

    @Autowired
    private VerificationRepository verificationRepository;

    @Autowired
    private PaymentRepository paymentRepository;

    @GetMapping("getAllVerification")
    public List<Verification> getAllVerification(){
        return verificationRepository.findAll();

    }

    @PostMapping("/payment/{paymentId}/getverifications")
    public Page<Verification> getAllVerificationByPaymentId(@PathVariable (value = "paymentId") Long paymentId,
                                                     Pageable pageable) {
        return verificationRepository.findByPaymentId(paymentId, pageable);
    }

    @PostMapping("/payment/{paymentId}/postverification")
    public Verification createVerification(@PathVariable (value = "paymentId") Long paymentId,
                                 @Valid @RequestBody Verification verification) {
        return paymentRepository.findById(paymentId).map(payment -> {
            verification.setPayment(payment);
            return verificationRepository.save(verification);
        }).orElseThrow(() -> new ResourceNotFoundException("PaymentId " + paymentId + " not found"));
    }
}
