package com.cindykitchen.cindykitchenbackend.admin.paymentuser.repository;

import com.cindykitchen.cindykitchenbackend.admin.paymentuser.model.Done;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface DoneRepository extends JpaRepository<Done, Long> {
    Page<Done> findByVerificationId(Long verificationId, Pageable pageable);
}
