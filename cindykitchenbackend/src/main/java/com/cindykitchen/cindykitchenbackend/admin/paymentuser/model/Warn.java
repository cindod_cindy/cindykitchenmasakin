package com.cindykitchen.cindykitchenbackend.admin.paymentuser.model;

import com.cindykitchen.cindykitchenbackend.auth.models.User;
import com.fasterxml.jackson.annotation.JsonIgnore;
import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;

@Entity
@Table(name = "warns")
public class Warn extends AuditModel{

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @NotNull
    @Size(max = 250)
    private String warn_variable;

    @NotNull
    @Size(max = 250)
    private String username;


    @ManyToOne(fetch = FetchType.LAZY, optional = false)
    @JoinColumn(name = "done_id", nullable = false)
    @OnDelete(action = OnDeleteAction.CASCADE)
    @JsonIgnore
    private Done done;

    public Warn() {
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getWarn_variable() {
        return warn_variable;
    }

    public void setWarn_variable(String warn_variable) {
        this.warn_variable = warn_variable;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public Done getDone() {
        return done;
    }

    public void setDone(Done done) {
        this.done = done;
    }
}
