package com.cindykitchen.cindykitchenbackend.admin.week.controller;

import com.cindykitchen.cindykitchenbackend.admin.paymentuser.exception.ResourceNotFoundException;
import com.cindykitchen.cindykitchenbackend.admin.week.model.Week;
import com.cindykitchen.cindykitchenbackend.admin.week.repository.WeekRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping("/cindykitchen/api/week")
public class WeekController {

    @Autowired
    private WeekRepository weekRepository;

    @GetMapping("/getweek")
    public List<Week> getAllWeek()
    {
        return weekRepository.findAll();
    }

    @PostMapping("/postweek")
    public Week createWeek(@Valid @RequestBody Week week) {
        return weekRepository.save(week);
    }


    @DeleteMapping("/deleteweek/{weekId}")
    public ResponseEntity<?> deleteWeek(@PathVariable Long weekId) {
        return weekRepository.findById(weekId).map(week -> {
            weekRepository.delete(week);
            return ResponseEntity.ok().build();
        }).orElseThrow(() -> new ResourceNotFoundException("WeekId " + weekId + " not found"));
    }

}
