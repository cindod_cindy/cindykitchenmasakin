package com.cindykitchen.cindykitchenbackend;

import com.cindykitchen.cindykitchenbackend.product.property.ProductStorageProperties;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.data.jpa.repository.config.EnableJpaAuditing;

@SpringBootApplication
@EnableJpaAuditing
@EnableConfigurationProperties({
		ProductStorageProperties.class
})
public class CindykitchenbackendApplication {

	public static void main(String[] args) {
		SpringApplication.run(CindykitchenbackendApplication.class, args);
	}

}
