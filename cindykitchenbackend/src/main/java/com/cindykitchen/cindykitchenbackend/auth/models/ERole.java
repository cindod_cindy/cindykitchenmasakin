package com.cindykitchen.cindykitchenbackend.auth.models;

public enum ERole {

    ROLE_SELLER,
    ROLE_BUYER,
    ROLE_ADMIN
}
