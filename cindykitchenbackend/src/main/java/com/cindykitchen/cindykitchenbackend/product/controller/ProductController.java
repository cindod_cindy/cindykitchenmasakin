package com.cindykitchen.cindykitchenbackend.product.controller;

import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

import com.cindykitchen.cindykitchenbackend.product.message.ResponseMessage;
import com.cindykitchen.cindykitchenbackend.product.message.ResponseProduct;
import com.cindykitchen.cindykitchenbackend.product.message.dto.ProductRequest;
import com.cindykitchen.cindykitchenbackend.product.model.Product;
import com.cindykitchen.cindykitchenbackend.product.service.ProductStorageService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.repository.query.Param;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import javax.validation.Valid;

@Controller
@RequestMapping("/cindykitchen/api/product")
public class ProductController {

    @Autowired
    private ProductStorageService productStorageService;

    @PostMapping("/upload")
    public ResponseEntity<ResponseMessage> uploadFile(@RequestParam("file") MultipartFile file, @Param("productname")String productname,
                                                      @Param("productdescription")String productdescription, @Param("sellername")String sellername,
                                                      @Param("productprice")String productprice, @Param("productlocation")String productlocation,
                                                      @Param("productavaliable")String productavaliable, @Param("productcontact")String productcontact) {
        String message = "";
        try {
            productStorageService.store(file,productname,productdescription,sellername,productprice,productlocation,productavaliable,productcontact);

            message = " Berhasil di Upload" ;
            return ResponseEntity.status(HttpStatus.OK).body(new ResponseMessage(message));



        } catch (Exception e) {
            message = "Could not upload the file: " + file.getOriginalFilename() + "!";
            return ResponseEntity.status(HttpStatus.EXPECTATION_FAILED).body(new ResponseMessage(message));
        }
    }

    @GetMapping("/files")
    public ResponseEntity<List<ResponseProduct>> getListFiles() {
        List<ResponseProduct> files = productStorageService.getAllFiles().map(product -> {
            String fileDownloadUri = ServletUriComponentsBuilder
                    .fromCurrentContextPath()
                    .path("/files/")
                    .path(product.getId())
                    .toUriString();

            return new ResponseProduct(
                    product.getName(),
                    fileDownloadUri,
                    product.getType(),
                    product.getData().length,
            product.getProductname(),
            product.getProductdescription(),
            product.getSellername(),
            product.getProductprice(),
            product.getProductlocation(),
            product.getProductavaliable(),
            product.getProductcontact());

        }).collect(Collectors.toList());

        return ResponseEntity.status(HttpStatus.OK).body(files);
    }



    @GetMapping("/files/getfiles")
    public ResponseEntity<List<ResponseProduct>> getListFiless(@RequestParam ("sellername") String sellername) {

        List<ResponseProduct> files = productStorageService.getAllFiless(sellername).map(product -> {
            String fileDownloadUri = ServletUriComponentsBuilder
                    .fromCurrentContextPath()
                    .path("/files/")
                    .path(product.getId())
                    .toUriString();

            return new ResponseProduct(
                    product.getName(),
                    fileDownloadUri,
                    product.getType(),
                    product.getData().length,
                    product.getProductname(),
                    product.getProductdescription(),
                    product.getSellername(),
                    product.getProductprice(),
                    product.getProductlocation(),
                    product.getProductavaliable(),
                    product.getProductcontact());

        }).collect(Collectors.toList());

        return ResponseEntity.status(HttpStatus.OK).body(files);
    }


//    @GetMapping("/files")
//    public ResponseEntity<List<ResponseProduct>> searchProduct(@RequestParam("query") String query) {
//        List<ResponseProduct> files = productStorageService.getProductSearch(query).map(product -> {
//            String fileDownloadUri = ServletUriComponentsBuilder
//                    .fromCurrentContextPath()
//                    .path("/files/")
//                    .path(product.getId())
//                    .toUriString();
//
//            return new ResponseProduct(
//                    product.getName(),
//                    fileDownloadUri,
//                    product.getType(),
//                    product.getData().length,
//                    product.getProductname(),
//                    product.getProductdescription(),
//                    product.getSellername(),
//                    product.getProductprice(),
//                    product.getProductlocation(),
//                    product.getProductavaliable(),
//                    product.getProductconatact());
//
//        }).collect(Collectors.toList());
//
//        return ResponseEntity.status(HttpStatus.OK).body(files);
//    }
//

    @GetMapping("/files/{id}")
    public ResponseEntity<byte[]> getFile(@PathVariable String id) {
        Product fileDB = productStorageService.getProduct(id);

        return ResponseEntity.ok()
                .header(HttpHeaders.CONTENT_DISPOSITION, "attachment; filename=\"" + fileDB.getName() + "\"")
                .body(fileDB.getData());
    }

//    @GetMapping("/book")
//    public ResponseEntity readBooks(@RequestParam(required = false) String productlocation) {
//        if (productlocation == null) {
//            return ResponseEntity.ok(productStorageService.getLocation(productlocation));
//        }
//        return ResponseEntity.ok(productStorageService.getLocation(productlocation));
//    }

    @GetMapping("/products/search/filter")
    public ResponseEntity getProductsWithFilter (@RequestParam("query") String query, Pageable pageable) {
        return ResponseEntity.ok(productStorageService.getAllFilesLocation(query, pageable));
    }

    @GetMapping("/search/productname")
    public ResponseEntity readBooks(@RequestParam(required = false) String productname) {
        if (productname == null) {
            return ResponseEntity.ok(productStorageService.findProductByName(productname));
        }
        return ResponseEntity.ok(productStorageService.findProductByName(productname));
    }
}
