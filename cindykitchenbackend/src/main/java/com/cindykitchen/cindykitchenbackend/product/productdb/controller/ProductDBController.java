package com.cindykitchen.cindykitchenbackend.product.productdb.controller;

import com.cindykitchen.cindykitchenbackend.product.message.dto.UserGetDataRequest;
import com.cindykitchen.cindykitchenbackend.product.productdb.model.ProductData;
import com.cindykitchen.cindykitchenbackend.product.productdb.model.dto.ProductRequest;
import com.cindykitchen.cindykitchenbackend.product.productdb.service.ProductDataService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Slf4j
@RestController
@RequestMapping("/cindykitchen/api/productDB")
public class ProductDBController {
    @Autowired
    ProductDataService productDataService;

    @PostMapping("/save/respon")
    public ProductData saveProductData(@RequestBody ProductRequest productRequest){
        return productDataService.saveProduct(productRequest);
    }

    @GetMapping("/getall/data")
    public List<ProductData> getAllProduct(@RequestBody UserGetDataRequest userGetDataRequest){
            if (userGetDataRequest.getToken() !=null && userGetDataRequest.getUsername() !=null){
                log.info("Password cannot be null");

            }
        return productDataService.getAllDataProduct(userGetDataRequest);
    }

    @GetMapping("/product/search/filter")
    public ResponseEntity readBooksWithFilter (@RequestParam("query") String query, Pageable pageable) {
        return ResponseEntity.ok(productDataService.filterProducts(query, pageable));
    }

    @GetMapping("/productData/search/filter")
    public ResponseEntity getProductWithFilter (@RequestParam("query") String query, Pageable pageable) {
        return ResponseEntity.ok(productDataService.filterProductData(query, pageable));
    }

    @GetMapping("/product/search/filterByName")
    public ResponseEntity getProductWithFilterName (@RequestParam("query") String query, Pageable pageable) {
        return ResponseEntity.ok(productDataService.filterProductName(query, pageable));
    }

}
