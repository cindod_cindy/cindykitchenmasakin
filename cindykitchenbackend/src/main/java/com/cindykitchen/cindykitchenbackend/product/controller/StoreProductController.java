package com.cindykitchen.cindykitchenbackend.product.controller;

import com.cindykitchen.cindykitchenbackend.product.payload.Product;
import com.cindykitchen.cindykitchenbackend.product.service.ProductServiceStorage;
import com.cindykitchen.cindykitchenbackend.product.service.ProductStorageService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.Resource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import javax.servlet.http.HttpServletRequest;
import java.io.IOException;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

@RestController
@RequestMapping("/cindykitchen/api/storeProduct")
public class StoreProductController {

    private static final Logger logger = LoggerFactory.getLogger(StoreProductController.class);

    @Autowired
    private ProductServiceStorage fileStorageService;


    private String productname;

    private String productdescription;

    private String sellername;

    private String productprice;

    private String productlocation;

    private String productavaliable;

    private String productcontact;


    @PostMapping("/uploadFile")
    public Product uploadFile(@RequestParam("file") MultipartFile file, @RequestParam("productname") String productname,
                              @RequestParam("productdescription") String productdescription, @RequestParam("sellername") String sellername,
                              @RequestParam("productprice") String  productprice, @RequestParam("productlocation") String productlocation,
                              @RequestParam("productavaliable") String productavaliable, @RequestParam("productcontact") String productcontact
                              ) {
        String fileName = fileStorageService.storeFile(file);

        String fileDownloadUri = ServletUriComponentsBuilder.fromCurrentContextPath()
                .path("/downloadFile/")
                .path(fileName)
                .toUriString();

        return new Product(fileName, fileDownloadUri,
                file.getContentType(), file.getSize(),productname,productdescription,sellername,
                productprice,productlocation,productavaliable,productcontact
                );
    }

//    @PostMapping("/uploadMultipleFiles")
//    public List<UploadFileResponse> uploadMultipleFiles(@RequestParam("files") MultipartFile[] files) {
//        return Arrays.asList(files)
//                .stream()
//                .map(file -> uploadFile(file))
//                .collect(Collectors.toList());
//    }

    @GetMapping("/downloadFile/{fileName:.+}")
    public ResponseEntity<Resource> downloadFile(@PathVariable String fileName, HttpServletRequest request) {
        // Load file as Resource
        Resource resource = fileStorageService.loadFileAsResource(fileName);

        // Try to determine file's content type
        String contentType = null;
        try {
            contentType = request.getServletContext().getMimeType(resource.getFile().getAbsolutePath());
        } catch (IOException ex) {
            logger.info("Could not determine file type.");
        }

        // Fallback to the default content type if type could not be determined
        if(contentType == null) {
            contentType = "application/octet-stream";
        }

        return ResponseEntity.ok()
                .contentType(MediaType.parseMediaType(contentType))
                .header(HttpHeaders.CONTENT_DISPOSITION, "attachment; filename=\"" + resource.getFilename() + "\"")
                .body(resource);
    }
}
