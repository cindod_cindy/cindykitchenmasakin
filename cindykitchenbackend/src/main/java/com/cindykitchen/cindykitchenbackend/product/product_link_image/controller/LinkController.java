package com.cindykitchen.cindykitchenbackend.product.product_link_image.controller;

import com.cindykitchen.cindykitchenbackend.product.product_link_image.link_service.LinkServiceGetInterface;
import com.cindykitchen.cindykitchenbackend.product.product_link_image.model.Link;
import com.cindykitchen.cindykitchenbackend.product.product_link_image.repository.LinkRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;


@Slf4j
@RestController
@RequestMapping("/cindykitchen/api/link")
public class LinkController {

    @Autowired
    private LinkServiceGetInterface linkServiceGetInterface;

    @GetMapping("/getLink")
    public List<Link> getProductList() {
        return linkServiceGetInterface.findAll();
    }

//    @GetMapping("/products/{productId}")
//    public Product getProduct(@PathVariable(value = "productId") Long productId) {
//        return productService.findById(productId).orElseThrow(() -> new ResourceNotFoundException("productId " + productId + " not found"));
//    }

    @PostMapping("/createLink")
    public String createProduct(@RequestBody Link link) {
        linkServiceGetInterface.save(link);
        return "Product added";
    }

//    @PutMapping("/products/{productId}")
//    public String updateProduct(@PathVariable(value = "productId") Long productId, @RequestBody Product product) {
//        return productService.findById(productId).map(p -> {
//            p.setName(product.getName());
//            p.setPrice(product.getPrice());
//            productService.save(p);
//            return "Product updated";
//        }).orElseThrow(() -> new ResourceNotFoundException("productId " + productId + " not found"));
//    }
//
//    @DeleteMapping("/products/{productId}")
//    public String deleteProduct(@PathVariable(value = "productId") Long productId) {
//        return productService.findById(productId).map(p -> {
//            productService.deleteById(productId);
//            return "Product deleted";
//        }).orElseThrow(() -> new ResourceNotFoundException("productId " + productId + " not found"));
//    }

}
