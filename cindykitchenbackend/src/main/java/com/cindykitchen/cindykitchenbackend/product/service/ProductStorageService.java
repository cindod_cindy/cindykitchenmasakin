package com.cindykitchen.cindykitchenbackend.product.service;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import com.cindykitchen.cindykitchenbackend.product.message.ResponseProduct;
import com.cindykitchen.cindykitchenbackend.product.message.dto.ProductRequest;
import com.cindykitchen.cindykitchenbackend.product.message.dto.ProductResponseSearch;
import com.cindykitchen.cindykitchenbackend.product.model.Product;
import com.cindykitchen.cindykitchenbackend.product.repository.ProductRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.util.FileSystemUtils;
import org.springframework.util.StringUtils;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import javax.persistence.EntityNotFoundException;

@Service
@Slf4j
public class ProductStorageService {
    @Autowired
    private ProductRepository productRepository;

    public Product store(MultipartFile file , String productname, String productdescription, String sellername,
                         String productprice, String productlocation, String productavaliable, String productcontact) throws IOException {
        String fileName = StringUtils.cleanPath(file.getOriginalFilename());
        var product = Product.builder()
                .name(fileName)
                .type(file.getContentType())
                .data(file.getBytes())
                .productname(productname)
                .productdescription(productdescription)
                .sellername(sellername)
                .productprice(productprice)
                .productlocation(productlocation)
                .productavaliable(productavaliable)
                .productcontact(productcontact)
                .build();

        return productRepository.save(product);
    }

    public Product getProduct(String id) {
        return productRepository.findById(id).get();
    }

    public Stream<Product> getAllFiles() {
        return productRepository.findAll().stream();
    }

    //for api search test
//    public Stream<Product> getProductSearch(String productlocation){
//      return  productRepository.findProductByproductlocation(productlocation).stream();
//
//    }
//
//    public Product getLocation(String productlocation) {
//        Optional<Product> productOptional = productRepository.findProductByproductlocation(productlocation);
//        if (productOptional.isPresent()) {
//            return productOptional.get();
//        }
//        throw new EntityNotFoundException("Cant find any book under given ISBN");
//    }


    public ProductResponseSearch getAllFilesLocation(String productlocation, Pageable pageable) {

//        Product productId= new Product();
//
//        String fileDownloadUri = ServletUriComponentsBuilder
//                .fromCurrentContextPath()
//                .path("/files/")
//                .path(productId.getId())
//                .toUriString();


        Page<Product> products = productRepository.findProductByProductlocationContains(productlocation, pageable);
        log.info("testt {}", products.getContent());

        return ProductResponseSearch.builder()
                .numberOfItems(products.getTotalElements()).numberOfPages(products.getTotalPages())
                .productList(products.getContent())
                .build();
    }

    public Product findProductByName(String productname) {
        Optional<Product> product = productRepository.findProductByproductname(productname);
        if (product.isPresent()) {
            return product.get();
        }
        throw new EntityNotFoundException("Cant find any product under given productname");
    }

    public Stream<Product> getAllFiless(String sellername) {

        return productRepository.findProductBysellername(sellername).stream();
    }



}

