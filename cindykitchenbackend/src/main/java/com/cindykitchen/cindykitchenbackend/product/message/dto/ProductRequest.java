package com.cindykitchen.cindykitchenbackend.product.message.dto;

import java.io.Serializable;

public class ProductRequest implements Serializable {

    private String productname;

    private String productdescription;

    private String sellername;

    private String productprice;

    private String productlocation;

    private String productavaliable;

    private String productconatact;

    public String getProductname() {
        return productname;
    }

    public void setProductname(String productname) {
        this.productname = productname;
    }

    public String getProductdescription() {
        return productdescription;
    }

    public void setProductdescription(String productdescription) {
        this.productdescription = productdescription;
    }

    public String getSellername() {
        return sellername;
    }

    public void setSellername(String sellername) {
        this.sellername = sellername;
    }

    public String getProductprice() {
        return productprice;
    }

    public void setProductprice(String productprice) {
        this.productprice = productprice;
    }

    public String getProductlocation() {
        return productlocation;
    }

    public void setProductlocation(String productlocation) {
        this.productlocation = productlocation;
    }

    public String getProductavaliable() {
        return productavaliable;
    }

    public void setProductavaliable(String productavaliable) {
        this.productavaliable = productavaliable;
    }

    public String getProductconatact() {
        return productconatact;
    }

    public void setProductconatact(String productconatact) {
        this.productconatact = productconatact;
    }
}
