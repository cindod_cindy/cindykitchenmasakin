package com.cindykitchen.cindykitchenbackend.product.product_link_image.link_service;


import com.cindykitchen.cindykitchenbackend.product.product_link_image.model.Link;
import com.cindykitchen.cindykitchenbackend.product.product_link_image.repository.LinkRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class LinkServiceGetInterface implements LinkService{
    @Autowired
    private LinkRepository linkRepository;

    @Override
    public Link save(Link link) {
        return linkRepository.save(link);
    }

    @Override
    public void deleteById(Long id) {
        linkRepository.deleteById(id);
    }

    @Override
    public Optional<Link> findById(Long id) {
        return linkRepository.findById(id);
    }

    @Override
    public List<Link> findAll() {
        return linkRepository.findAll();
    }
}
