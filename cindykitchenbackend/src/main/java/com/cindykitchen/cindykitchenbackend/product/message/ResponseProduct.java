package com.cindykitchen.cindykitchenbackend.product.message;

public class ResponseProduct {

    private String name;
    private String url;
    private String type;
    private long size;

    private String productname;

    private String productdescription;

    private String sellername;

    private String productprice;

    private String productlocation;

    private String productavaliable;

    private String productconatact;

    public ResponseProduct(String name, String url, String type, long size, String productname, String productdescription, String sellername, String productprice, String productlocation, String productavaliable, String productconatact) {
        this.name = name;
        this.url = url;
        this.type = type;
        this.size = size;
        this.productname = productname;
        this.productdescription = productdescription;
        this.sellername = sellername;
        this.productprice = productprice;
        this.productlocation = productlocation;
        this.productavaliable = productavaliable;
        this.productconatact = productconatact;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public long getSize() {
        return size;
    }

    public void setSize(long size) {
        this.size = size;
    }

    public String getProductname() {
        return productname;
    }

    public void setProductname(String productname) {
        this.productname = productname;
    }

    public String getProductdescription() {
        return productdescription;
    }

    public void setProductdescription(String productdescription) {
        this.productdescription = productdescription;
    }

    public String getSellername() {
        return sellername;
    }

    public void setSellername(String sellername) {
        this.sellername = sellername;
    }

    public String getProductprice() {
        return productprice;
    }

    public void setProductprice(String productprice) {
        this.productprice = productprice;
    }

    public String getProductlocation() {
        return productlocation;
    }

    public void setProductlocation(String productlocation) {
        this.productlocation = productlocation;
    }

    public String getProductavaliable() {
        return productavaliable;
    }

    public void setProductavaliable(String productavaliable) {
        this.productavaliable = productavaliable;
    }

    public String getProductconatact() {
        return productconatact;
    }

    public void setProductconatact(String productconatact) {
        this.productconatact = productconatact;
    }
}
