package com.cindykitchen.cindykitchenbackend.product.payload;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.Type;

import javax.persistence.*;

public class Product {

    private String fileName;
    private String fileDownloadUri;
    private String fileType;
    private long size;


    private String productname;

    private String productdescription;

    private String sellername;

    private String productprice;

    private String productlocation;

    private String productavaliable;

    private String productcontact;

    public Product(String fileName, String fileDownloadUri, String fileType, long size, String productname, String productdescription, String sellername, String productprice, String productlocation, String productavaliable, String productcontact) {
        this.fileName = fileName;
        this.fileDownloadUri = fileDownloadUri;
        this.fileType = fileType;
        this.size = size;
        this.productname = productname;
        this.productdescription = productdescription;
        this.sellername = sellername;
        this.productprice = productprice;
        this.productlocation = productlocation;
        this.productavaliable = productavaliable;
        this.productcontact = productcontact;
    }

    public String getFileName() {
        return fileName;
    }

    public void setFileName(String fileName) {
        this.fileName = fileName;
    }

    public String getFileDownloadUri() {
        return fileDownloadUri;
    }

    public void setFileDownloadUri(String fileDownloadUri) {
        this.fileDownloadUri = fileDownloadUri;
    }

    public String getFileType() {
        return fileType;
    }

    public void setFileType(String fileType) {
        this.fileType = fileType;
    }

    public long getSize() {
        return size;
    }

    public void setSize(long size) {
        this.size = size;
    }

    public String getProductname() {
        return productname;
    }

    public void setProductname(String productname) {
        this.productname = productname;
    }

    public String getProductdescription() {
        return productdescription;
    }

    public void setProductdescription(String productdescription) {
        this.productdescription = productdescription;
    }

    public String getSellername() {
        return sellername;
    }

    public void setSellername(String sellername) {
        this.sellername = sellername;
    }

    public String getProductprice() {
        return productprice;
    }

    public void setProductprice(String productprice) {
        this.productprice = productprice;
    }

    public String getProductlocation() {
        return productlocation;
    }

    public void setProductlocation(String productlocation) {
        this.productlocation = productlocation;
    }

    public String getProductavaliable() {
        return productavaliable;
    }

    public void setProductavaliable(String productavaliable) {
        this.productavaliable = productavaliable;
    }

    public String getProductcontact() {
        return productcontact;
    }

    public void setProductcontact(String productcontact) {
        this.productcontact = productcontact;
    }
}
