package com.cindykitchen.cindykitchenbackend.product.productdb.model;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;

@Entity
@Table(name = "product_data")
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class ProductData {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private String fileDownloadUri;

    private String productname;

    private String productdescription;

    private String sellername;

    private String productprice;

    private String productlocation;

    private String productavaliable;

    private String productcontact;


}
