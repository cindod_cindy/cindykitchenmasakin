package com.cindykitchen.cindykitchenbackend.product.productdb.repository;

import com.cindykitchen.cindykitchenbackend.product.productdb.model.ProductData;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface ProductDataRepository extends JpaRepository<ProductData,Long> {
    Page<ProductData> findAllProductDataByproductname(String productname, Pageable pageable);

    List<ProductData> findAllProductDataByproductnameContains(String productname, Pageable pageable);
}
