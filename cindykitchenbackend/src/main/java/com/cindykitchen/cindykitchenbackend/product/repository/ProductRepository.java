package com.cindykitchen.cindykitchenbackend.product.repository;

import com.cindykitchen.cindykitchenbackend.admin.paymentuser.model.Done;
import com.cindykitchen.cindykitchenbackend.product.model.Product;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface ProductRepository extends JpaRepository<Product, String> {


    //Optional<Product> findProductByproductlocation(String productlocation);

    Page<Product> findProductByProductlocation(String productlocation ,Pageable pageable);
    Page<Product> findProductByProductlocationContains(String productlocation ,Pageable pageable);

    Optional<Product> findProductByproductname(String productname);

    List<Product> findProductBysellername(String sellername);

   /* Page<Product> findAllByProductNameContains(String productname, Pageable pageable);

    Page<Product> findAllByProductPriceContains(String productprice, Pageable pageable);

    Page<Product> findAllByProductAvaliableContains(String productavaliable, Pageable pageable);

    */
}
