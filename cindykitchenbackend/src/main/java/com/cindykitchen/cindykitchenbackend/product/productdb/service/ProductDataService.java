package com.cindykitchen.cindykitchenbackend.product.productdb.service;

import com.cindykitchen.cindykitchenbackend.product.message.dto.UserGetDataRequest;
import com.cindykitchen.cindykitchenbackend.product.productdb.model.ProductData;
import com.cindykitchen.cindykitchenbackend.product.productdb.model.dto.FilterByNameResponse;
import com.cindykitchen.cindykitchenbackend.product.productdb.model.dto.PaginatedProductResponse;
import com.cindykitchen.cindykitchenbackend.product.productdb.model.dto.ProductRequest;
import com.cindykitchen.cindykitchenbackend.product.productdb.model.dto.ProductSearchRespon;
import com.cindykitchen.cindykitchenbackend.product.productdb.repository.ProductDataRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class ProductDataService {

    @Autowired
    ProductDataRepository productDataRepository;

    public ProductData saveProduct(ProductRequest productRequest){

         var saveProducts = ProductData.builder()
                 .fileDownloadUri(productRequest.getFileDownloadUri())
                 .productname(productRequest.getProductname())
                 .productdescription(productRequest.getProductdescription())
                 .sellername(productRequest.getSellername())
                 .productprice(productRequest.getProductprice())
                 .productlocation(productRequest.getProductlocation())
                 .productavaliable(productRequest.getProductavaliable())
                 .productcontact(productRequest.getProductcontact())
                 .build();
        ProductData saveProductData = productDataRepository.saveAndFlush(saveProducts);

        return saveProductData;
    }

    public List<ProductData> getAllDataProduct(UserGetDataRequest userGetDataRequest){
            List<ProductData> productDataList = productDataRepository.findAll();
            return  productDataList;

    }


    public List<FilterByNameResponse> filterProducts(String productname, Pageable pageable) {

        Page<ProductData> productList = productDataRepository.findAllProductDataByproductname(productname, pageable);
        List<FilterByNameResponse> filterByNameResponses = new ArrayList<>();
        productList.forEach(productData -> {
//
            filterByNameResponses.add(FilterByNameResponse.builder()
                            .fileDownloadUri(productData.getFileDownloadUri())
                            .productname(productData.getProductname())
                            .productdescription(productData.getProductdescription())
                            .sellername(productData.getSellername())
                            .productprice(productData.getProductprice())
                            .productlocation(productData.getProductlocation())
                            .productavaliable(productData.getProductavaliable())
                            .productcontact(productData.getProductcontact())
                    .build());

        });
        return filterByNameResponses;
    }

    public PaginatedProductResponse filterProductData(String productname, Pageable pageable) {

        Page<ProductData> productData = productDataRepository.findAllProductDataByproductname(productname, pageable);
        return PaginatedProductResponse.builder()
                .numberOfItems(productData.getTotalElements()).numberOfPages(productData.getTotalPages())
                .productDataList(productData.getContent())
                .build();
    }


    public List<ProductSearchRespon> filterProductName(String productname, Pageable pageable){

        List<ProductData> productDataList = productDataRepository.findAllProductDataByproductnameContains(productname, pageable);
        List<ProductSearchRespon> productSearchRespons= new ArrayList<>();
        productDataList.forEach(productData ->{

            productSearchRespons.add(ProductSearchRespon.builder()
                    .id(productData.getId())
                    .fileDownloadUri(productData.getFileDownloadUri())
                    .productname(productData.getProductname())
                    .productdescription(productData.getProductdescription())
                    .sellername(productData.getSellername())
                    .productprice(productData.getProductprice())
                    .productlocation(productData.getProductlocation())
                    .productavaliable(productData.getProductavaliable())
                    .productcontact(productData.getProductcontact())
                    .build());

        });
        return productSearchRespons;
    }


}
