package com.cindykitchen.cindykitchenbackend.product.message.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class UserGetDataRequest {

    public  String token;
    public String username;

}
