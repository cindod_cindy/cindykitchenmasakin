package com.cindykitchen.cindykitchenbackend.product.message.dto;

import com.cindykitchen.cindykitchenbackend.product.model.Product;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@Builder
public class ProductResponseSearch {

    private List<Product> productList;
    private Long numberOfItems;
    private int numberOfPages;
}
