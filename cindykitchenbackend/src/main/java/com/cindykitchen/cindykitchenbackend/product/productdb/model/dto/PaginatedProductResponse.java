package com.cindykitchen.cindykitchenbackend.product.productdb.model.dto;

import com.cindykitchen.cindykitchenbackend.product.productdb.model.ProductData;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class PaginatedProductResponse {
    private List<ProductData> productDataList;
    private Long numberOfItems;
    private int numberOfPages;
}
