package com.cindykitchen.cindykitchenbackend.product.product_link_image.repository;

import com.cindykitchen.cindykitchenbackend.product.product_link_image.model.Link;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface LinkRepository extends JpaRepository<Link, Long> {
}
