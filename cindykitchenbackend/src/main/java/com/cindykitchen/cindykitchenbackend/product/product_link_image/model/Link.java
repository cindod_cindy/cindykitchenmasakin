package com.cindykitchen.cindykitchenbackend.product.product_link_image.model;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import javax.validation.constraints.NotNull;

@Entity
@Table(name = "links")
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class Link {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @NotNull
    private String fileName;

    @NotNull
    private String fileDownloadUri;

    @NotNull
    private String fileType;

    @NotNull
    private long size;

    @NotNull
    private String productname;

    @NotNull
    private String productdescription;

    @NotNull
    private String sellername;

    @NotNull
    private String productprice;

    @NotNull
    private String productlocation;

    @NotNull
    private String productavaliable;

    @NotNull
    private String productcontact;


    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getFileName() {
        return fileName;
    }

    public void setFileName(String fileName) {
        this.fileName = fileName;
    }

    public String getFileDownloadUri() {
        return fileDownloadUri;
    }

    public void setFileDownloadUri(String fileDownloadUri) {
        this.fileDownloadUri = fileDownloadUri;
    }

    public String getFileType() {
        return fileType;
    }

    public void setFileType(String fileType) {
        this.fileType = fileType;
    }

    public long getSize() {
        return size;
    }

    public void setSize(long size) {
        this.size = size;
    }

    public String getProductname() {
        return productname;
    }

    public void setProductname(String productname) {
        this.productname = productname;
    }

    public String getProductdescription() {
        return productdescription;
    }

    public void setProductdescription(String productdescription) {
        this.productdescription = productdescription;
    }

    public String getSellername() {
        return sellername;
    }

    public void setSellername(String sellername) {
        this.sellername = sellername;
    }

    public String getProductprice() {
        return productprice;
    }

    public void setProductprice(String productprice) {
        this.productprice = productprice;
    }

    public String getProductlocation() {
        return productlocation;
    }

    public void setProductlocation(String productlocation) {
        this.productlocation = productlocation;
    }

    public String getProductavaliable() {
        return productavaliable;
    }

    public void setProductavaliable(String productavaliable) {
        this.productavaliable = productavaliable;
    }

    public String getProductcontact() {
        return productcontact;
    }

    public void setProductcontact(String productcontact) {
        this.productcontact = productcontact;
    }
}
