package com.cindykitchen.cindykitchenbackend.product.product_link_image.link_service;

import com.cindykitchen.cindykitchenbackend.product.product_link_image.model.Link;

import java.util.List;
import java.util.Optional;

public interface LinkService {
    Link save(Link link);

    void deleteById(Long id);

    Optional<Link> findById(Long id);

    List<Link> findAll();
}
